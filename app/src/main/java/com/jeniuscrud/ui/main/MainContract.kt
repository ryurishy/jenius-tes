package com.jeniuscrud.ui.main

import com.jeniuscrud.model.ContactData

class MainContract {

    interface Presenter{
        fun getDataContact()
        fun stop()
    }

    interface View{
        fun showProgress()
        fun hideProgress()
        fun onSuccess(contact: List<ContactData>)
        fun onError(error: Throwable)
    }
}