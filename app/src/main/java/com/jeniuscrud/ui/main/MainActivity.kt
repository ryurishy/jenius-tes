package com.jeniuscrud.ui.main

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.jeniuscrud.R
import com.jeniuscrud.model.ContactData
import com.jeniuscrud.ui.detail.DetailActivity
import com.jeniuscrud.ui.input.InputActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.ctx
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import java.util.ArrayList

class MainActivity : AppCompatActivity(), MainContract.View {

    private var presenter: MainPresenter? = null
    private var adapter: MainAdapter? = null
    private var linearLayoutManager: LinearLayoutManager? = null
    private var arrayList = ArrayList<ContactData>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initUI()
        initToolbar()
        presenter = MainPresenter(this)
        presenter?.getDataContact()

    }

    private fun initToolbar(){
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(true)
    }

    override fun onResume() {
        super.onResume()
        presenter?.getDataContact()

    }
    private fun initUI(){

        float_contact.setOnClickListener {
            ctx.startActivity<InputActivity>()
        }

        linearLayoutManager = LinearLayoutManager(this)
        rcy_contact.setHasFixedSize(true)
        rcy_contact.layoutManager = linearLayoutManager
        rcy_contact.fitsSystemWindows = true
    }

    override fun showProgress() {
        rl_progressbar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        rl_progressbar.visibility = View.GONE
    }

    override fun onSuccess(contact: List<ContactData>) {
        arrayList.clear()
        if (contact.isNotEmpty()){
            arrayList.addAll(contact)
            adapter = arrayList.let {
                MainAdapter(this,it){
                    ctx.startActivity<DetailActivity>("id" to "${it.id}")
                }
            }
            rcy_contact.adapter = adapter
        }
    }

    override fun onError(error: Throwable) {
//        toast("Something went wrong")
        toast(error.message.toString())
    }
}
