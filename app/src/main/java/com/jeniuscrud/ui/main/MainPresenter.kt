package com.jeniuscrud.ui.main

import com.jeniuscrud.network.ApiClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.annotations.NonNull
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class MainPresenter(private val view: MainContract.View): MainContract.Presenter {

    @NonNull
    var disposable: Disposable? = null

    override fun getDataContact() {
        view.showProgress()
        disposable = ApiClient.instance
            .getAllContact()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                res ->
                view.hideProgress()
                res.data?.let { view.onSuccess(it) }
            },{
                error ->
                view.hideProgress()
                view.onError(error)
            })
    }

    override fun stop() {
       if (disposable != null){
           disposable?.dispose()
       }
    }

}