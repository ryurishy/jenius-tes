package com.jeniuscrud.ui.main

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jeniuscrud.R
import com.jeniuscrud.model.ContactData
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_contact.*

class MainAdapter(private val context: Context, private val items: ArrayList<ContactData>, private val listener: (ContactData) -> Unit):
    RecyclerView.Adapter<MainAdapter.ViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int) =
        ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_contact, p0, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, p1: Int) {
        holder.bindItem(items[p1], listener, context)
    }

    class ViewHolder(override val containerView: View): RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bindItem(items: ContactData, listener: (ContactData) -> Unit, context: Context){
            txt_first_name.text = items.firstName
            txt_last_name.text = items.lastName
            txt_age.text = items.age.toString()

            containerView.setOnClickListener {
                listener(items)
            }

        }

    }

}