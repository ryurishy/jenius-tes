package com.jeniuscrud.ui.detail

import com.jeniuscrud.network.ApiClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.annotations.NonNull
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class DetailPresenter(private val view: DetailContract.View):DetailContract.Presenter {

    @NonNull
    var disposable: Disposable? = null

    override fun getDetailContact(id: String) {
        view.showProgress()
        disposable = ApiClient.instance
            .getContactById(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                res ->
                view.hideProgress()
                res.data?.let { view.onSuccess(it) }
            },{
                error->
                view.hideProgress()
                view.onError(error)
            })
    }

    override fun deleteContact(id: String) {
       view.showProgress()
        disposable = ApiClient.instance
            .deleteContact(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                res ->
                view.hideProgress()
                res.message?.let { view.onSuccessDelete(it) }
            },{
                error ->
                view.hideProgress()
                view.onError(error)
            })
    }

    override fun stop() {
        if (disposable != null){
            disposable?.dispose()
        }
    }
}