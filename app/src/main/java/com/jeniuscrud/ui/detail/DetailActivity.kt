package com.jeniuscrud.ui.detail

import android.app.ProgressDialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import com.jeniuscrud.R
import com.jeniuscrud.model.ContactData
import com.jeniuscrud.ui.edit.EditActivity
import kotlinx.android.synthetic.main.activity_detail.*
import org.jetbrains.anko.ctx
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class DetailActivity : AppCompatActivity(), DetailContract.View{

    private var presenter: DetailPresenter? = null
    private var progressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        initToolbar()
        presenter = DetailPresenter(this)
        val id = intent.getStringExtra("id")
        presenter?.getDetailContact(id)

        btn_delete.setOnClickListener {
            presenter?.deleteContact(id)
        }
    }

    private fun initToolbar(){
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        progressDialog = ProgressDialog(this)
        progressDialog?.setMessage("Loading...")
        progressDialog?.setProgressStyle(ProgressDialog.STYLE_SPINNER)

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                finish()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun showProgress() {
//        pb_detail.visibility = View.VISIBLE
        progressDialog?.show()
    }

    override fun hideProgress() {
//        pb_detail.visibility = View.GONE
        progressDialog?.dismiss()
    }

    override fun onSuccess(contact: ContactData) {
       txt_first_name.text = contact.firstName
        txt_last_name.text = contact.lastName
        txt_age.text = contact.age.toString()

        btn_edit.setOnClickListener {
            ctx.startActivity<EditActivity>("contact_data" to contact)
            finish()
        }
    }

    override fun onSuccessDelete(message: String) {
        toast(message)
        finish()
    }

    override fun onError(error: Throwable) {
//        toast("Something went wrong")
        toast(error.message.toString())
    }
}