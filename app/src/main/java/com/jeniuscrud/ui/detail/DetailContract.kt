package com.jeniuscrud.ui.detail

import com.jeniuscrud.model.ContactData

class DetailContract {

    interface Presenter{
        fun getDetailContact(id: String)
        fun deleteContact(id: String)
        fun stop()
    }

    interface View{
        fun showProgress()
        fun hideProgress()
        fun onSuccess(contact: ContactData)
        fun onSuccessDelete(message: String)
        fun onError(error: Throwable)
    }
}