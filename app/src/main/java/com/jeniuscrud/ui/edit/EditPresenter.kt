package com.jeniuscrud.ui.edit

import com.jeniuscrud.model.form.ContactForm
import com.jeniuscrud.network.ApiClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.annotations.Nullable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class EditPresenter(private val view: EditContract.View): EditContract.Presenter {
    @Nullable
    var disposable: Disposable? = null
    override fun editContact(id: String, contactForm: ContactForm) {
        view.showProgress()
        disposable = ApiClient.instance
            .editContact(id,contactForm)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                res ->
                view.hideProgress()
                res.message?.let { view.onSuccess(it) }
            },{
                error ->
                view.hideProgress()
                view.onError(error)
            })
    }

    override fun stop() {
        if (disposable != null){
            disposable?.dispose()
        }
    }
}