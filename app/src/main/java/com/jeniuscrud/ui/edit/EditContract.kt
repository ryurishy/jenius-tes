package com.jeniuscrud.ui.edit

import com.jeniuscrud.model.form.ContactForm

class EditContract {

    interface Presenter{
        fun editContact(id: String, contactForm: ContactForm)
        fun stop()
    }

    interface View{
        fun showProgress()
        fun hideProgress()
        fun onSuccess(message: String)
        fun onError(error: Throwable)
    }
}