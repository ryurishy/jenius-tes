package com.jeniuscrud.ui.edit

import android.app.ProgressDialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import com.jeniuscrud.R
import com.jeniuscrud.model.ContactData
import com.jeniuscrud.model.form.ContactForm
import kotlinx.android.synthetic.main.activity_edit.*
import org.jetbrains.anko.toast

class EditActivity : AppCompatActivity(),EditContract.View {

    private var presenter: EditPresenter? = null
    private var firstName: String = ""
    private var lastName: String = ""
    private var age: String = ""
    private var id: String = ""
    private var progressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)
        initToolbar()
        presenter = EditPresenter(this)

        val data = intent.getParcelableExtra<ContactData>("contact_data")
        setupUI(data)

    }

    private fun initToolbar(){
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        progressDialog = ProgressDialog(this)
        progressDialog?.setMessage("Loading...")
        progressDialog?.setProgressStyle(ProgressDialog.STYLE_SPINNER)

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                finish()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }


    private fun setupUI(contactData: ContactData){
        id = contactData.id.toString()
        input_first_name.setText(contactData.firstName)
        input_last_name.setText(contactData.lastName)
        input_age.setText(contactData.age.toString())

        btn_edit.setOnClickListener {
            firstName = input_first_name.text.toString()
            lastName = input_last_name.text.toString()
            age = input_age.text.toString()

            presenter?.editContact(id, ContactForm(firstName,lastName,age.toInt()))
        }
    }

    override fun showProgress() {
//        pb_detail.visibility = View.VISIBLE
        progressDialog?.show()
    }

    override fun hideProgress() {
//       pb_detail.visibility = View.GONE
        progressDialog?.dismiss()
    }

    override fun onSuccess(message: String) {
       toast(message)
        finish()
    }

    override fun onError(error: Throwable) {
//        toast("Something went wrong")
        toast(error.message.toString())
    }
}