package com.jeniuscrud.ui.input

import com.jeniuscrud.model.form.ContactForm
import com.jeniuscrud.network.ApiClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.annotations.Nullable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class InputPresenter(private val view: InputContract.View): InputContract.Presenter {

    @Nullable
    var disposable: Disposable? = null
    override fun postContact(contactForm: ContactForm) {
        view.showProgress()
        disposable = ApiClient.instance
            .postContact(contactForm)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                res ->
                view.hideProgress()
                res.message?.let { view.onSuccess(it) }
            },{
                error ->
                view.hideProgress()
                view.onError(error)
            })
    }

    override fun stop() {
        if (disposable != null){
            disposable?.dispose()
        }
    }
}