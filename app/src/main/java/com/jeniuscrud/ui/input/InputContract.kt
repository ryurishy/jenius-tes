package com.jeniuscrud.ui.input

import com.jeniuscrud.model.form.ContactForm

class InputContract {

    interface Presenter{
        fun postContact(contactForm: ContactForm)
        fun stop()
    }

    interface View{
        fun showProgress()
        fun hideProgress()
        fun onSuccess(message: String)
        fun onError(error: Throwable)
    }
}