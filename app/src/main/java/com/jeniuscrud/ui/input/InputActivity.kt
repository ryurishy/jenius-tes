package com.jeniuscrud.ui.input

import android.app.ProgressDialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import com.jeniuscrud.R
import com.jeniuscrud.model.form.ContactForm
import kotlinx.android.synthetic.main.activity_input.*
import org.jetbrains.anko.toast

class InputActivity : AppCompatActivity(), InputContract.View {

    private var presenter: InputPresenter? = null
    private var firstName: String = ""
    private var lastName: String = ""
    private var age: String = ""
    private var progressDialog: ProgressDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_input)
        initToolbar()

        presenter = InputPresenter(this)

        postContact()

    }

    private fun postContact(){

        btn_create.setOnClickListener {
            firstName = input_first_name.text.toString()
            lastName = input_last_name.text.toString()
            age = input_age.text.toString()

            presenter?.postContact(ContactForm(firstName,lastName,age.toInt()))
        }



    }

    private fun initToolbar(){
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        progressDialog = ProgressDialog(this)
        progressDialog?.setMessage("Loading...")
        progressDialog?.setProgressStyle(ProgressDialog.STYLE_SPINNER)
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                finish()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun showProgress() {
        //        pb_detail.visibility = View.VISIBLE
        progressDialog?.show()
    }

    override fun hideProgress() {
        //       pb_detail.visibility = View.GONE
        progressDialog?.dismiss()
    }

    override fun onSuccess(message: String) {
       toast(message)
        finish()
    }

    override fun onError(error: Throwable) {
//        toast("Something went wrong")
        toast(error.message.toString())
    }
}