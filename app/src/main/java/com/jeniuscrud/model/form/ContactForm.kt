package com.jeniuscrud.model.form

data class ContactForm (val firstName: String, val lastName: String, val age: Int)