package com.jeniuscrud.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ContactEditResponse(

    @field:SerializedName("data")
    val data: ContactData? = null,

    @field:SerializedName("message")
    val message: String? = null
):Parcelable