package com.jeniuscrud.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ContactPostResponse(

    @field:SerializedName("message")
    val message: String? = null
):Parcelable