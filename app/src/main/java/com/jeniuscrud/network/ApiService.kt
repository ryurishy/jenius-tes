package com.jeniuscrud.network

import com.jeniuscrud.model.*
import com.jeniuscrud.model.form.ContactForm
import io.reactivex.Observable
import retrofit2.http.*

interface ApiService {

        @GET("contact")
        fun getAllContact():Observable<ContactGetResponse>

        @GET("contact/{id}")
        fun getContactById(@Path("id") id:String): Observable<ContactDetailResponse>

        @POST("contact")
        fun postContact(@Body contactForm: ContactForm):Observable<ContactPostResponse>

        @DELETE("contact/{id}")
        fun deleteContactById(@Path("id") id:String): Observable<ContactDeleteResponse>

        @PUT("contact/{id}")
        fun editContactById(@Path("id") id:String, @Body contactForm: ContactForm): Observable<ContactEditResponse>

}