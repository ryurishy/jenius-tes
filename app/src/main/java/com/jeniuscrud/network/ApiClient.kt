package com.jeniuscrud.network

import com.google.gson.GsonBuilder
import com.jeniuscrud.BuildConfig
import com.jeniuscrud.model.*
import com.jeniuscrud.model.form.ContactForm
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

class ApiClient {

    private val apiService: ApiService

    init {
        val gson = GsonBuilder()
            .setLenient()
            .create()

        val clientBuilder = OkHttpClient.Builder()
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        clientBuilder.addInterceptor(loggingInterceptor)

        val retrofit = Retrofit.Builder().baseUrl(BuildConfig.BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(getUnsafeOkHttpClient())
            .build()

        apiService = retrofit.create(ApiService::class.java)
    }

    private fun getUnsafeOkHttpClient(): OkHttpClient {
        try {
            // Create a trust manager that does not validate certificate chains
            val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
                override fun getAcceptedIssuers(): Array<X509Certificate> {
                    return arrayOf()
                }
                @Throws(CertificateException::class)
                override fun checkClientTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
                }

                @Throws(CertificateException::class)
                override fun checkServerTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
                }
            })

            // Install the all-trusting trust manager
            val sslContext = SSLContext.getInstance("SSL")
            sslContext.init(null, trustAllCerts, java.security.SecureRandom())
            // Create an ssl socket factory with our all-trusting manager
            val sslSocketFactory = sslContext.socketFactory

            val builder = OkHttpClient.Builder()
            builder.sslSocketFactory(sslSocketFactory)
            builder.hostnameVerifier { _, _ -> true }

            return builder.build()
        } catch (e: Exception) {
            throw RuntimeException(e)
        }

    }


    companion object {
        private var apiClient: ApiClient? = null

        val instance : ApiClient get() {
            if (apiClient == null){
                apiClient = ApiClient()
            }
            return apiClient as ApiClient
        }
    }

    fun getAllContact():Observable<ContactGetResponse>{
        return apiService.getAllContact()
    }

    fun getContactById(id: String):Observable<ContactDetailResponse>{
        return apiService.getContactById(id)
    }

    fun postContact(contactForm: ContactForm):Observable<ContactPostResponse>{
        return apiService.postContact(contactForm)
    }

    fun deleteContact(id: String):Observable<ContactDeleteResponse>{
        return apiService.deleteContactById(id)
    }

    fun editContact(id: String, contactForm: ContactForm):Observable<ContactEditResponse>{
        return apiService.editContactById(id,contactForm)
    }


}